It was craziness of highest order when first human (or one of their ancestor) tried the standing up posture. Everyone looked at them in surprise and disgust. The only reason for survival was to hunt and feed yourself and then being able to mount the perfect mate. Why on earth will you try to stand up on two legs. And what is it that you will do with the other two hanging in the air. The wise came up to tell them that legs are meant to be on the ground. Always ; except when you lie belly down to sleep. In fact, horses never agreed to let their guard down. The majestic runner kept on four feet even during the sleep. Losing ground below four feet, to them, was losing the life. 

It was not that everyone in humans were convinced. There seemed no merit in halving down your ability to run. It was fighting the design at the very core. If the standing up on two feet was doable, walking and running on just two was absolute pain. The other group suggested that "monkey model" was lot better. There were three distinct benefits in addition to easily climbing up the trees for survival and fruits.

- Monkeys could see far beyond from the tree tops
- They could easily climb up other fast movers such as horses and cows and use them to cover the distance.
- They could use the hands to somewhat throw the stones.

Others hailed that monkeys are anyway our forefathers, there was absolutely nothing new we could do by simply giving up two legs while on the ground. There seemed no applications of "Not using two legs" as legs when you are on the ground. 
